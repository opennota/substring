substring [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](https://godoc.org/gitlab.com/opennota/substring?status.svg)](http://godoc.org/gitlab.com/opennota/substring) [![Pipeline status](https://gitlab.com/opennota/substring/badges/master/pipeline.svg)](https://gitlab.com/opennota/substring/commits/master)
=========

Package substring implements case-insensitive substring matching using Rabin-Karp algorithm.

## Install

    go get -u gitlab.com/opennota/substring
